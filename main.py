from datetime import datetime
import os

from flask import Flask, render_template, request, redirect, url_for, session
from passlib.hash import pbkdf2_sha256

from model import Task, User

app = Flask(__name__)
app.secret_key = b'\x9d\xb1u\x08%\xe0\xd0p\x9bEL\xf8JC\xa3\xf4J(hAh\xa4\xcdw\x12S*,u\xec\xb8\xb8'


@app.route('/all')
def all_tasks():
    return render_template('all.jinja2', tasks=Task.select())


@app.route('/create', methods=['GET', 'POST'])
def create_task():
    if 'username' not in session:
        return redirect(url_for('login'))

    if request.method == 'POST':
        new_task = Task(name=request.form['task-name'])
        new_task.save()
        return redirect(url_for('all_tasks'))
    else:
        return render_template('create.jinja2')


@app.route('/login', methods=['GET', 'POST'])
def do_login():
    if request.method == 'POST':
        login_user = User.select().where(User.name == request.form['name']).get()
        
        if login_user and pbkdf2_sha256.verify(request.form['password'], login_user.password):
            session['username'] = request.form['name']
            return redirect(url_for('all_tasks'))
        else:
            return render_template('login.jinja2', error="Incorrect username or password.")
    else:
        return render_template('login.jinja2') 


@app.route('/incomplete', methods=['GET', 'POST'])
def incomplete_tasks():
    if 'username' not in session:
        return redirect(url_for('login'))

    if request.method == 'POST':
        incomplete_user = User.select().where(User.name == session['username']).get()
        Task.update(done=datetime.now(), done_by=incomplete_user)\
            .where(Task.id == request.form['task_id'])\
            .execute()

    incomplete_task_list = Task.select().where(Task.done.is_null())

    return render_template('incomplete.jinja2', tasks=incomplete_task_list)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
